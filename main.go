package main

import (
	log "github.com/sirupsen/logrus"
	"preauthbridge/internal/api"
	"preauthbridge/internal/config"
	"preauthbridge/internal/database"
	"preauthbridge/internal/security"
)

func main() {
	err := config.LoadConfig()
	if err != nil {
		log.Fatal(err)
	}

	dbConfig := &config.CurrentPreAuthBridgeConfig.Database
	dbConnection, err := database.New(
		dbConfig.Username,
		dbConfig.Password,
		dbConfig.Host,
		dbConfig.Port,
		dbConfig.Db,
	)
	if err != nil {
		log.Fatal(err)
	}

	auth := security.New(dbConnection)

	api.Listen(auth)
}
