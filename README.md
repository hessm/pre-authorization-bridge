# Pre Authorization Bridge

> Service for implementing two-factor authentication for credential offering

two possibilities for authentication:
1. only authentication code
2. authentication code and pin (two-factor)

every authentication have a configurable time-to-live(ttl)

## Configuration
````yaml
protocol: nats
nats:
  url: http://localhost:4222
  timeoutInSec: 10

logLevel: debug
servingPort: 3001
databaseUrl: redis://user:pass@localhost:6379/0
defaultTtlInMin: 30
oAuth:
  serverUrl: http://hydra:4444
  clientId: bridge
  clientSecret: secret
wellKnown:
  issuer: http://localhost:8080
  token_endpoint: http://localhost:8080/token
  grant_types_supported: urn:ietf:params:oauth:grant-type:pre-authorized_code
````

## Usage
Bridge expects cloudevent to trigger pre-authentication with following structure:
````go
type offerEvent struct {
    TwoFactor struct {
        Enabled          bool   `mapstructure:"enabled"`
        RecipientType    string `mapstructure:"recipientType"` //only when Enabled=true
        RecipientAddress string `mapstructure:"recipientAddress"` //only when Enabled=true
    } `mapstructure:"twoFactor"`
    Ttl time.Duration `mapstructure:"ttl"`
}
````

## Authorization Server
### hydra
````bash
hydra create client --endpoint http://localhost:4445/ --grant-type client_credentials --token-endpoint-auth-method client_secret_post
````

