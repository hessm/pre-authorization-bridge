package api

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/cloudevents/sdk-go/v2/event"
	log "github.com/sirupsen/logrus"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"preauthbridge/internal/config"
	"regexp"
	"sync"
	"time"
)

type RecipientType string

const didComm RecipientType = "didComm"
const email RecipientType = "email"
const didCommRegex = "did:[a-z]+:[A-Za-z0-9]+"
const emailRegex = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$"

type twoFactorOffer struct {
	Pin              string
	RecipientType    RecipientType
	RecipientAddress string
}

type offerEvent struct {
	TwoFactor struct {
		Enabled          bool          `mapstructure:"enabled"`
		RecipientType    RecipientType `mapstructure:"recipientType"`
		RecipientAddress string        `mapstructure:"recipientAddress"`
	} `mapstructure:"twoFactor"`
	Ttl time.Duration `mapstructure:"ttl" `
}

type didCommEvent struct {
	Subject        string `json:"subject"`
	DidCommAddress string `json:"didCommAddress"`
	Body           string `json:"body"`
}

type emailEvent struct {
	Subject      string `json:"subject"`
	EmailAddress string `json:"to-email"`
	Body         string `json:"body"`
}

var reqClient *cloudeventprovider.CloudEventProviderClient

func processOffersHandler(ctx context.Context, event event.Event) (*event.Event, error) {
	log.Debugf("received offer event: %v", event)
	var eventData offerEvent
	if err := json.Unmarshal(event.Data(), &eventData); err != nil {
		log.Error(err)
		return nil, err
	}

	ttl := time.Duration(config.CurrentPreAuthBridgeConfig.DefaultTtlInMin) * time.Minute
	if eventData.Ttl != 0 {
		ttl = eventData.Ttl
	}
	newAuth, err := auth.Generate(ctx, eventData.TwoFactor.Enabled, ttl)
	if err != nil {
		err = fmt.Errorf("error occured while generate new authentication: %w", err)
		log.Error(err)
		return nil, err
	}

	replyData, err := json.Marshal(newAuth.Code)
	if err != nil {
		log.Error(err)
		return nil, err
	}
	replyEvent, err := cloudeventprovider.NewEvent("pre-auth-bridge", "auth.offer.v1", replyData)
	if err != nil {
		err = fmt.Errorf("error occured while creating reply event: %w", err)
		log.Error(err)
		return nil, err
	}

	twoFactor := eventData.TwoFactor
	if twoFactor.Enabled {
		isValid, err := checkTwoFactor(twoFactor.RecipientType, twoFactor.RecipientAddress)
		if err != nil {
			err = fmt.Errorf("error occured while checking two-factor offer: %w", err)
			log.Error(err)
			return nil, err
		}
		if isValid {
			offer := twoFactorOffer{
				Pin:              newAuth.Pin,
				RecipientType:    eventData.TwoFactor.RecipientType,
				RecipientAddress: eventData.TwoFactor.RecipientAddress,
			}

			err := processTwoFactor(offer)
			if err != nil {
				err = fmt.Errorf("error occured while processing two-factor offer: %w", err)
				log.Error(err)
				return nil, err
			}
		} else {
			err = fmt.Errorf("recipientAddress %s does not match recipientType %s", twoFactor.RecipientAddress, twoFactor.RecipientType)
			log.Error(err)
			return nil, err
		}
	}

	log.Debugf("reply with authCode: %s", newAuth.Code)
	return &replyEvent, nil
}

func processTwoFactor(offer twoFactorOffer) error {
	var eventType string
	var data json.RawMessage
	var err error
	switch offer.RecipientType {
	case didComm:
		eventType = "didcomm.pin.v1"
		data, err = json.Marshal(didCommEvent{
			Subject:        "two-factor pin",
			DidCommAddress: offer.RecipientAddress,
			Body:           offer.Pin,
		})
	case email:
		eventType = "email.pin.v1"
		data, err = json.Marshal(emailEvent{
			Subject:      "two-factor pin",
			EmailAddress: offer.RecipientAddress,
			Body:         offer.Pin,
		})
	}
	if err != nil {
		err = fmt.Errorf("error while creating twoFactorEvent for recipientAddress: %s: %w", offer.RecipientAddress, err)
		log.Errorf("%v", err)
		return err
	}

	newEvent, err := cloudeventprovider.NewEvent("preauthbridge/handleOffer", eventType, data)
	if err != nil {
		log.Errorf("error while creating twoFactorOffer for twoFactorOffer publishing: %v", newEvent)
	}

	log.Debugf("send request with pinCode: %s", offer.Pin)
	_, err = reqClient.Request(newEvent, 30*time.Second)
	if err != nil {
		err = fmt.Errorf("error while publish pin event %v for two-factor: %w", newEvent, err)
		log.Errorf("%v", err)
		return err
	}
	return nil
}

func checkTwoFactor(recipientType RecipientType, recipientAddress string) (bool, error) {
	if recipientType != "" && recipientAddress != "" {
		var match bool
		var err error
		switch recipientType {
		case didComm:
			match, err = regexp.Match(didCommRegex, []byte(recipientAddress))
		case email:
			match, err = regexp.Match(emailRegex, []byte(recipientAddress))
		default:
			match = false
			err = fmt.Errorf("recipientType %s is not valid", recipientType)
		}
		if err != nil {
			return false, fmt.Errorf("unexpected error while checking regex: %w", err)
		}
		return match, nil
	}
	return false, fmt.Errorf("not all twoFactor fields set! Type: %s, Address: %s", recipientType, recipientAddress)
}

func startMessaging(wg *sync.WaitGroup) {
	log.Info("start messaging!")

	defer wg.Done()

	repClient, err := cloudeventprovider.NewClient(cloudeventprovider.Rep, config.CurrentPreAuthBridgeConfig.OfferTopic)
	if err != nil {
		panic(err)
	}
	defer repClient.Close()

	reqClient, err = cloudeventprovider.NewClient(cloudeventprovider.Req, config.CurrentPreAuthBridgeConfig.TwoFactorTopic)
	if err != nil {
		panic(err)
	}
	defer reqClient.Close()

	if err := repClient.Reply(processOffersHandler); err != nil {
		panic(err)
	}
}
