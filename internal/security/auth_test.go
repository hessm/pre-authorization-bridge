package security

import (
	"context"
	"preauthbridge/internal/database"
	"preauthbridge/internal/entity"
	"testing"
	"time"
)

const pinAuthCode = "PINrtz46s5tg4hj2sdu7"
const noPinAuthCode = "NOPINz46s5tg4hj2sdu7"
const ttlExpiredAuthCode = "TTLg5z46s5tg4hj2sdu7"
const validPin = "123456"

var mockData map[string]string

var mockDatabaseConnection MockDatabaseConnection

type MockDatabaseConnection struct{}

func (m *MockDatabaseConnection) SaveAuth(ctx context.Context, authentication *entity.Authentication, ttl time.Duration) error {
	return nil
}

func (m *MockDatabaseConnection) GetPinOfAuthCode(ctx context.Context, authCode string) (string, error) {
	if authCode == ttlExpiredAuthCode {
		return "", database.ErrKeyNotFound
	} else {
		return mockData[authCode], nil
	}
}

func (m *MockDatabaseConnection) DeletePinOfAuthCode(ctx context.Context, authCode string) (bool, error) {
	return true, nil
}

func init() {
	mockData = map[string]string{
		pinAuthCode:   validPin,
		noPinAuthCode: "",
	}

	mockDatabaseConnection = MockDatabaseConnection{}
}

func TestGenerateAuthWithPin(t *testing.T) {
	testingAuth := New(&mockDatabaseConnection)

	authentication, err := testingAuth.Generate(context.Background(), true, 24*time.Hour)
	if err != nil {
		t.Errorf("Unexpected error occured: %v", err)
	}

	if authentication.Pin == "" {
		t.Fatalf("expect pin not empty")
	}
}

func TestGenerateAuthWithoutPin(t *testing.T) {
	testingAuth := New(&mockDatabaseConnection)

	authentication, err := testingAuth.Generate(context.Background(), false, 24*time.Hour)
	if err != nil {
		t.Errorf("Unexpected error occured: %v", err)
	}

	if authentication.Pin != "" {
		t.Fatalf("expect pin to be empty")
	}
}

func TestCheckWithValidPin(t *testing.T) {
	testingAuth := New(&mockDatabaseConnection)

	validAuthentication := entity.Authentication{
		Code: pinAuthCode,
		Pin:  validPin,
	}

	isValid, err := testingAuth.Check(context.Background(), &validAuthentication)
	if err != nil {
		t.Errorf("Unexpected error occured: %v", err)
	}

	if !isValid {
		t.Fatalf("expect authentication to be valid")
	}
}

func TestCheckWithWrongPin(t *testing.T) {
	testingAuth := New(&mockDatabaseConnection)

	validAuthentication := entity.Authentication{
		Code: pinAuthCode,
		Pin:  "000000",
	}

	isValid, err := testingAuth.Check(context.Background(), &validAuthentication)
	if err != nil {
		t.Errorf("Unexpected error occured: %v", err)
	}

	if isValid {
		t.Fatalf("expect authentication to be invalid")
	}
}

func TestCheckTtlExpired(t *testing.T) {
	testingAuth := New(&mockDatabaseConnection)

	validAuthentication := entity.Authentication{
		Code: ttlExpiredAuthCode,
		Pin:  "",
	}

	isValid, err := testingAuth.Check(context.Background(), &validAuthentication)
	if err != nil {
		t.Errorf("Unexpected error occured: %v", err)
	}

	if isValid {
		t.Fatalf("expect authentication to be invalid")
	}
}

func TestCheckNoPin(t *testing.T) {
	testingAuth := New(&mockDatabaseConnection)

	validAuthentication := entity.Authentication{
		Code: noPinAuthCode,
		Pin:  "",
	}

	isValid, err := testingAuth.Check(context.Background(), &validAuthentication)
	if err != nil {
		t.Errorf("Unexpected error occured: %v", err)
	}

	if !isValid {
		t.Fatalf("expect authentication to be valid")
	}
}
