package database

import (
	"context"
	"fmt"
	"github.com/redis/go-redis/v9"
	"github.com/sethvargo/go-retry"
	log "github.com/sirupsen/logrus"
	"preauthbridge/internal/entity"
	"time"
)

type redisConnection struct {
	client *redis.Client
}

func newRedisConnection(username string, password string, host string, port int, db string) (*redisConnection, error) {
	connUrl := fmt.Sprintf("redis://%s:%s@%s:%d/%s", username, password, host, port, db)
	opt, err := redis.ParseURL(connUrl)
	if err != nil {
		return nil, fmt.Errorf("could not parse connUrl '%s': %w", connUrl, err)
	}

	newClient := redis.NewClient(opt)

	bf := retry.NewFibonacci(time.Millisecond * 500)
	bf = retry.WithCappedDuration(time.Second*30, bf)
	bf = retry.WithJitter(time.Millisecond*50, bf)
	bf = retry.WithMaxDuration(time.Minute*2, bf)

	if err = retry.Do(context.Background(), bf, func(_ context.Context) error {
		if _, err := newClient.Ping(context.Background()).Result(); err != nil {
			err = retry.RetryableError(fmt.Errorf("connection check failed: error: %w", err))
			log.Error(err)

			return err
		}

		return nil
	}); err != nil {

		return nil, err
	}

	return &redisConnection{
		client: newClient,
	}, nil
}

func (r *redisConnection) SaveAuth(ctx context.Context, authentication *entity.Authentication, ttl time.Duration) error {
	if err := r.client.Set(ctx, authentication.Code, authentication.Pin, ttl).Err(); err != nil {
		return err
	}
	return nil
}

func (r *redisConnection) GetPinOfAuthCode(ctx context.Context, authCode string) (string, error) {
	pin, err := r.client.Get(ctx, authCode).Result()
	if err == redis.Nil {
		return "", ErrKeyNotFound
	} else if err != nil {
		return "", err
	}
	return pin, nil
}

func (r *redisConnection) DeletePinOfAuthCode(ctx context.Context, authCode string) (bool, error) {
	result, err := r.client.Del(ctx, authCode).Result()
	if err == redis.Nil {
		return false, ErrKeyNotFound
	} else if err != nil {
		return false, err
	}

	if result > 0 {
		return true, nil
	}

	return false, nil
}
